import { createApp } from 'vue'
import App from './App.vue'
import './css/index.css'
import Vuex from 'vuex'

const store = new Vuex.Store({
    state: {
      record: null
    },
    getters: {
      isOpen (state) {
        return (state.record !== null)
      },
      read (state) {
          return state.record
      }
    },
    mutations: {
      open (state, row) {
        state.record = row
      },
      close (state) {
        state.record = null
      }
    }
  })

createApp(App).use(store).mount('#app')
