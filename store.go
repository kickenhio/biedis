package main

import (
	"time"
)

type Database struct {
	values map[string]string
	ttl    map[string]int64
}

type Store struct {
	db map[int64]Database
}

func NewStore() Store {
	store := Store{
		db: make(map[int64]Database),
	}

	go store.garbageCollector()

	return store
}

func newDatabase() Database {
	return Database{
		values: make(map[string]string),
		ttl:    make(map[string]int64),
	}
}

func (store *Store) Databases() map[int64]Database {
	return store.db
}

func (store *Store) FlushAll() bool {
	for _, db := range store.db {
		db.Flush()
	}

	return true
}

func (store *Store) Select(idx int64) *Database {
	db, ok := store.db[idx]
	if !ok {
		db = newDatabase()
		store.db[idx] = db
	}

	return &db
}

func (db *Database) All() map[string]string {
	return db.values
}

func (db *Database) Ttl() map[string]int64 {
	return db.ttl
}

func (db *Database) Get(key string) (string, bool) {
	val, ok := db.values[key]
	return val, ok
}

func (db *Database) Delete(key string) bool {
	_, exists := db.Get(key)

	delete(db.values, key)
	delete(db.ttl, key)

	return exists
}

func (db *Database) Flush() {
	for key := range db.values {
		db.Delete(key)
	}
}

func (db *Database) Set(key string, value string) Database {
	db.values[key] = value

	return *db
}

func (db *Database) SetEx(key string, value string, ttl int64) Database {
	timestamp := time.Now().Unix()

	db.values[key] = value
	db.ttl[key] = timestamp + ttl

	return *db
}

func (store *Store) garbageCollector() {
	ticker := time.NewTicker(time.Second)
	quit := make(chan struct{})

	for {
		select {
		case <-ticker.C:
			store.cleanup()
		case <-quit:
			ticker.Stop()
		}
	}
}

func (store *Store) cleanup() {
	timestamp := time.Now().Unix()

	for _, db := range store.db {
		for key, time := range db.ttl {
			if timestamp >= time {
				db.Delete(key)
			}
		}
	}
}
