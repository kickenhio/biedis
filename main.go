package main

import (
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/bsm/redeo"
	"github.com/bsm/redeo/resp"
	"github.com/gin-gonic/gin"
)

var mutex sync.RWMutex
var store Store
var manager Manager

func init() {
	store = NewStore()
	manager = NewManager()
	mutex = sync.RWMutex{}
}

type Element struct {
	Index string `json:"index"`
	Value Value  `json:"value"`
}

type Property struct {
	Name      string `json:"name"`
	Accesible bool   `json:"accessible"`
	Value     Value  `json:"value"`
}

type Value struct {
	Atomic     string     `json:"type"`
	Value      string     `json:"value,omitempty"`
	Properties []Property `json:"properties,omitempty"`
	Items      []Element  `json:"items,omitempty"`
}

func decompile(value string) Value {
	if value[1:2] == ":" {
		position := scanNodeFinishIndex(value)

		if position < 1 {
			log.Fatal("nie znaleziono")
		}

		return extractValueFromNode(value[:position])
	}

	return Value{Atomic: "string", Value: value}
}

func extractValueFromNode(value string) Value {
	x := Value{}

	switch value[0:2] {
	case "N;":
		x.Atomic = "null"
	case "s:":
		x.Atomic = "string"
		offset := strings.Index(value[2:], ":")
		x.Value = value[4+offset : len(value)-1]
	case "O:":
		beginning := strings.Index(value[2:], ":")
		ending := strings.Index(value[beginning+4:], ":")
		x.Atomic = value[beginning+4 : beginning+3+ending]

		begin := strings.Index(value, "{")
		end := scanNodeFinishIndex(value)
		x.Properties = processProperties(value[begin:end])
	case "i:":
		x.Atomic = "integer"
		offset := strings.Index(value, ":")
		x.Value = value[offset+1:]
	case "a:":
		x.Atomic = "array"
		begin := strings.Index(value, "{")
		end := scanNodeFinishIndex(value)
		x.Items = processArrayItems(value[begin:end])
	case "b:":
		x.Atomic = "boolean"
		offset := strings.Index(value, ":")
		if value[offset+1:] == "1" {
			x.Value = "1"
		} else {
			x.Value = "0"
		}

	}

	return x
}

func processProperties(value string) []Property {
	result := []Property{}
	workEl := Property{}
	isName := true

	for len(value) >= 2 {
		switch value[0:2] {
		case "i:", "O:", "s:", "a:", "b:", "N;":
			stop := scanNodeFinishIndex(value)
			node := extractValueFromNode(value[:stop])

			if isName {
				workEl.Name = node.Value
			} else {
				workEl.Value = node
			}

			value = value[stop:]

			if !isName {
				result = append(result, workEl)
				workEl = Property{}
			}

			isName = !isName
		default:
			value = value[1:]
		}
	}

	return result
}

func processArrayItems(value string) []Element {
	result := []Element{}
	workEl := Element{}
	isKey := true

	for len(value) >= 2 {
		switch value[0:2] {
		case "i:", "O:", "s:", "a:", "b:", "N;":
			stop := scanNodeFinishIndex(value)
			node := extractValueFromNode(value[:stop])

			if isKey {
				workEl.Index = node.Value
			} else {
				workEl.Value = node
			}

			value = value[stop:]

			if !isKey {
				result = append(result, workEl)
				workEl = Element{}
			}

			isKey = !isKey
		default:
			value = value[1:]
		}
	}

	return result
}

func scanNodeFinishIndex(value string) int {
	switch value[0:2] {
	case "i:":
		search := value[2:]
		for i := 0; i < len(search); i++ {
			if _, err := strconv.Atoi(search[i : i+1]); err != nil {
				return i + 2
			}
		}
		return -1
	case "b:":
		return 2
	case "N;":
		return 2
	case "s:":
		offset := strings.Index(value, "\"")
		finish := strings.Index(value[offset+1:], "\"")

		return offset + finish + 2
	case "a:", "O:":
		begin := strings.Index(value, "{")
		search := value[begin+1:]
		x := 1

		for i := 0; i < len(search); i++ {
			if search[i:i+1] == "{" {
				x++
			}

			if search[i:i+1] == "}" {
				x--
			}

			if x == 0 {
				return (i + begin + 2)
			}
		}

	}

	return -1
}

func main() {
	gin.SetMode(gin.ReleaseMode)

	dzin := gin.New()
	dzin.Use(gin.Recovery())

	dzin.NoRoute(func(c *gin.Context) {
		c.JSON(404, gin.H{
			"code": "NOT_FOUND",
		})
	})

	ex, err := os.Executable()
	if err != nil {
		log.Fatal(err)
	}

	dir := filepath.Dir(ex)

	PATH_SEP := string(filepath.Separator)

	if err != nil {
		log.Fatal(err)
	}

	dzin.Static("/css", dir+PATH_SEP+"my-project"+PATH_SEP+"dist"+PATH_SEP+"css")
	dzin.Static("/js", dir+PATH_SEP+"my-project"+PATH_SEP+"dist"+PATH_SEP+"js")
	dzin.Static("/img", dir+PATH_SEP+"my-project"+PATH_SEP+"dist"+PATH_SEP+"img")

	dzin.LoadHTMLFiles(dir + PATH_SEP + "my-project" + PATH_SEP + "dist" + PATH_SEP + "index.html")

	dzin.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.html", nil)
	})

	dzin.GET("/api/databases", func(c *gin.Context) {
		x := make(map[int64]int)
		for idx, db := range store.Databases() {
			x[idx] = len(db.values)
		}

		c.JSON(200, x)
	})

	dzin.GET("/api/database/:idx/entries", func(c *gin.Context) {
		x := make(map[string]map[string]string)
		timestamp := time.Now().Unix()

		idx, err := strconv.Atoi(c.Param("idx"))
		if err != nil {
			idx = 0
		}

		idx64 := int64(idx)

		ttl := store.Select(idx64).Ttl()

		for key, value := range store.Select(idx64).All() {
			ttltime, ok := ttl[key]
			entry := make(map[string]string)
			entry["key"] = key
			entry["value"] = value

			if ok {
				entry["ttl"] = strconv.Itoa(int(ttltime - timestamp))
			}

			x[key] = entry
		}

		c.JSON(200, x)
	})

	dzin.GET("/api/database/:idx/:key", func(c *gin.Context) {
		idx, err := strconv.Atoi(c.Param("idx"))
		if err != nil {
			idx = 0
		}

		value, exists := store.Select(int64(idx)).Get(c.Param("key"))
		if !exists {
			value = "empty"
		}

		c.JSON(200, decompile(value))
	})

	go dzin.Run(":8081")

	if err := runRedis(); err != nil {
		log.Fatalln(err)
	}
}

func runRedis() error {
	broker := redeo.NewPubSubBroker()
	srv := redeo.NewServer(nil)

	srv.Handle("ping", redeo.Ping())
	srv.Handle("echo", redeo.Echo())
	srv.Handle("info", redeo.Info(srv))
	srv.Handle("set", broker.Publish())
	srv.Handle("subscribe", broker.Subscribe())

	srv.HandleFunc("select", func(w resp.ResponseWriter, c *resp.Command) {
		idx, err := c.Arg(0).Int()

		client := redeo.GetClient(c.Context())

		if err != nil {
			log.Fatal(err)
		}

		manager.Set(client.ID(), idx)

		w.AppendOK()
	})

	srv.HandleFunc("del", func(w resp.ResponseWriter, c *resp.Command) {
		client := redeo.GetClient(c.Context())
		dbIdx := manager.Get(client.ID())

		currentStore := store.Select(dbIdx)
		sum := int64(0)

		for x := 0; x > c.ArgN(); x++ {
			key := c.Arg(x).String()
			if currentStore.Delete(key) {
				sum++
			}
		}

		w.AppendInt(sum)
	})

	srv.HandleFunc("setex", func(w resp.ResponseWriter, c *resp.Command) {
		key := c.Arg(0).String()
		ttl := c.Arg(1).String()
		val := c.Arg(2).String()

		intTtl, err := strconv.ParseInt(ttl, 10, 64)

		if err != nil {
			panic(err)
		}

		client := redeo.GetClient(c.Context())
		dbIdx := manager.Get(client.ID())

		mutex.Lock()
		store.Select(dbIdx).SetEx(key, val, intTtl)
		mutex.Unlock()

		w.AppendInt(1)
	})

	srv.HandleFunc("sadd", func(w resp.ResponseWriter, c *resp.Command) {
		if c.ArgN() != 2 {
			w.AppendError(redeo.WrongNumberOfArgs(c.Name))
			return
		}

		w.AppendInt(1)
	})

	srv.HandleFunc("hset", func(w resp.ResponseWriter, c *resp.Command) {
		if c.ArgN() != 3 {
			w.AppendError(redeo.WrongNumberOfArgs(c.Name))
			return
		}

		w.AppendInt(1)
	})

	srv.HandleFunc("hget", func(w resp.ResponseWriter, c *resp.Command) {
		if c.ArgN() != 2 {
			w.AppendError(redeo.WrongNumberOfArgs(c.Name))
			return
		}

		w.AppendInt(1)
	})

	srv.HandleFunc("smembers", func(w resp.ResponseWriter, c *resp.Command) {

		fmt.Println("SMEMBERS " + c.Arg(0).String())

		w.AppendInt(1)
	})

	srv.HandleFunc("set", func(w resp.ResponseWriter, c *resp.Command) {
		if c.ArgN() != 2 {
			w.AppendError(redeo.WrongNumberOfArgs(c.Name))
			return
		}

		key := c.Arg(0).String()
		val := c.Arg(1).String()

		client := redeo.GetClient(c.Context())
		dbIdx := manager.Get(client.ID())

		mutex.Lock()
		store.Select(dbIdx).Set(key, val)
		mutex.Unlock()

		w.AppendInt(1)
	})

	srv.HandleFunc("flushdb", func(w resp.ResponseWriter, c *resp.Command) {
		client := redeo.GetClient(c.Context())
		dbIdx := manager.Get(client.ID())

		store.Select(dbIdx).Flush()

		w.AppendOK()
	})

	srv.HandleFunc("flushall", func(w resp.ResponseWriter, c *resp.Command) {
		store.FlushAll()

		w.AppendOK()
	})

	srv.HandleFunc("get", func(w resp.ResponseWriter, c *resp.Command) {
		if c.ArgN() != 1 {
			w.AppendError(redeo.WrongNumberOfArgs(c.Name))
			return
		}

		key := c.Arg(0).String()

		client := redeo.GetClient(c.Context())
		dbIdx := manager.Get(client.ID())

		mutex.RLock()
		val, ok := store.Select(dbIdx).Get(key)
		mutex.RUnlock()

		if ok {
			w.AppendBulkString(val)
			return
		}

		w.AppendNil()
	})

	lis, err := net.Listen("tcp", ":6378")
	if err != nil {
		return err
	}

	defer lis.Close()

	log.Printf("waiting for connections on %s", lis.Addr().String())
	return srv.Serve(lis)
}
