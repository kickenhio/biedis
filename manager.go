package main

import (
	"time"
)

type Manager struct {
	pool map[uint64]int64
	ttl  map[uint64]int64
}

func NewManager() Manager {
	manager := Manager{
		pool: make(map[uint64]int64),
		ttl:  make(map[uint64]int64),
	}

	go manager.garbageCollector()

	return manager
}

func (manager *Manager) garbageCollector() {
	ticker := time.NewTicker(time.Second)
	quit := make(chan struct{})

	for {
		select {
		case <-ticker.C:
			manager.cleanup()
		case <-quit:
			ticker.Stop()
		}
	}
}

func (manager *Manager) cleanup() {
	timestamp := time.Now().Unix()

	for key, time := range manager.ttl {
		if timestamp >= time {
			delete(manager.pool, key)
			delete(manager.ttl, key)
		}
	}
}

func (manager *Manager) Get(connId uint64) int64 {
	val := manager.pool[connId]
	return val
}

func (manager *Manager) Set(connId uint64, dbId int64) bool {
	manager.pool[connId] = dbId
	manager.ttl[connId] = time.Now().Add(time.Minute).Unix()
	return true
}
